//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "sam.h"
#include "tlc5940.h"
#include "tlc_counter.h"
#include "tlc_timer.h"
#include "cie1931.h"
#include "spi.h"

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define SPI_MOSI (PORT_PB10)
#define SPI_MOSI_GROUP (1)
#define SPI_MOSI_PIN (PIN_PB10%32)
#define SPI_MOSI_PMUX (SPI_MOSI_PIN/2)

#define SPI_MISO (PORT_PA12)
#define SPI_MISO_GROUP (0)
#define SPI_MISO_PIN (PIN_PA12%32)
#define SPI_MISO_PMUX (SPI_MISO_PIN/2)

#define SPI_SCK (PORT_PB11)
#define SPI_SCK_GROUP (1)
#define SPI_SCK_PIN (PIN_PB11%32)
#define SPI_SCK_PMUX (SPI_SCK_PIN/2)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

static volatile bool spiBusy = false;
static volatile bool tlc_send = false; 

static uint8_t byteCounter = 1;
static volatile uint8_t spiData[24];

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

void channel_init();
static void convert_array(uint16_t data[], uint8_t newData[]);
void tlc_isr();

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
void tlc_init()
{	
	tlc_timer_init();
	tlc_counter_init();
	channel_init();
	tlc_timer_enable();
	tlc_counter_enable();
}


//==============================================================================
void tlc_write(uint16_t data[16])
{
	if(spi_lock(SPI_LOCK_TLC5940) == SPI_LOCK_TLC5940 && !tlc_send){
		
		// Turn on interrupts
		SERCOM4->SPI.INTENSET.bit.TXC = 1;
		
		// get the array of bytes
		convert_array(data, spiData);
		
		byteCounter = 1;
		// Send the first byte
		tlc_send = true;
		spi_register_isr(tlc_isr);
		SERCOM4->SPI.DATA.bit.DATA = spiData[0];
	}
}

bool tlc_lock()
{
	__disable_irq();
	bool retval = true;
	
	if(spiBusy)
	retval = false;
	else
	spiBusy = true;
	
	tlc_send = retval;
	
	__enable_irq();
	return retval;
}

void tlc_unlock()
{	
	tlc_send = false;
}


//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

void channel_init()
{
	// Enable the bus clock for the Event System
	PM->APBCMASK.bit.EVSYS_ = 1;

	// Configure the General Clock with the 48MHz clk
	GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(GCLK_CLKCTRL_ID_EVSYS_0) |
	GCLK_CLKCTRL_GEN_GCLK0 |
	GCLK_CLKCTRL_CLKEN;
	// Wait for the GCLK to be synchronized
	while(GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);

	// Reset the event system
	EVSYS->CTRL.bit.SWRST = 1;
	
	// Use Channel 0 - Note that 1 must be added to the channel in the USER
	// Set up the User as TC3 (0x12)
	EVSYS->USER.reg = EVSYS_USER_CHANNEL(0+1) | EVSYS_USER_USER(0x12);
	
	// Set up the channel generator as TCC0_MCX0 (0x25)
	// The Async and Sync paths both work, but the SINGLE edges don't seem to
	EVSYS->CHANNEL.reg = EVSYS_CHANNEL_CHANNEL(0) |
	EVSYS_CHANNEL_EDGSEL_RISING_EDGE |
	EVSYS_CHANNEL_PATH_ASYNCHRONOUS |
	EVSYS_CHANNEL_EVGEN(0x25);
	
}


// converts uint16_t array with 16, 12 bit words in it
// into a uint8_t array with 24 bytes
static void convert_array(uint16_t data[], uint8_t newData[])
{
	// words in this case are 12 bit long, right-adjusted values
	uint8_t wordCount = 15;
	uint8_t byteCount = 0;
	
	for(int i = 0; i < 24; i++)
	newData[i] = 0;
	
	while(wordCount <= 15){
		newData[byteCount] = led_lookup[data[wordCount]] >> 4;
		byteCount++;
		
		newData[byteCount] = (led_lookup[data[wordCount]] & 0xF) << 4;
		wordCount--;
		newData[byteCount] |= led_lookup[data[wordCount]] >> 8;
		byteCount++;
		
		newData[byteCount] = led_lookup[data[wordCount]] & 0xFF;
		byteCount++;
		wordCount--;
	}
}

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------
void tlc_isr()
{	
	if(byteCounter < 24){
		SERCOM4->SPI.DATA.bit.DATA = spiData[byteCounter];
		byteCounter++;
	}
	else{
		latch_plz();
	}
}