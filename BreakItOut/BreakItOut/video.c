//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "video.h"
#include "spi.h"
#include "sam.h"
#include "delay.h"
#include <string.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define VIDEO_RST (PORT_PA18)
#define VIDEO_RST_GROUP (0)
#define VIDEO_RST_PIN (PIN_PA18%32)

#define VIDEO_CS (PORT_PA21)
#define VIDEO_CS_GROUP (0)
#define VIDEO_CS_PIN (PIN_PA21%32)

#define NO_OP                           0x0000
#define DISPLAY_DUTY                    0x0001
#define	RGB_INTERFACE					0x0002
#define	ENTRY_MODE						0x0003
#define	DISPLAY_CONTROL1				0x0005
#define	STAND_BY						0x0010
#define	OSC_REG							0x0018
#define	POWER_CONTROL3					0x00F8
#define	POWER_CONTROL4					0x00F9
#define	GRAM_ADDRESS_SET_X				0x0020
#define	GRAM_ADDRESS_SET_Y				0x0021
#define	GRAM_DATA_WRITE					0x0022
#define	GRAM_DATA_READ					0x0022
#define	IF_8BIT_BUS						0x0024
#define	VER_WINDOW_BEG					0x0035
#define	VER_WINDOW_END					0x0036
#define	HOR_WINDOW_BEGEND				0x0037
#define	GAMMA_TOPBOT_R					0x0070
#define	GAMMA_TOPBOT_G					0x0071
#define	GAMMA_TOPBOT_B					0x0072
#define	GAMMA_R12						0x0073
#define	GAMMA_R34						0x0074
#define	GAMMA_G12						0x0075
#define	GAMMA_G34						0x0076
#define	GAMMA_B12						0x0077
#define	GAMMA_B34						0x0078
#define	GAMMA_Q							0x0080

#define VIDCMD_VIDEO (0x01)
#define VIDCMD_INDEX (0x02)
#define VIDCMD_PARAM (0x04)

#define VID(x,y)		((VIDCMD_VIDEO << 24) | ((x)<<16) | (y))
#define VID_INDEX(x)	((VIDCMD_INDEX << 24) | ((x)<<16))
#define VID_PARAM(y)	((VIDCMD_PARAM << 24) |  (y))


//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

typedef enum
{
	VIDEO_PRELUDE,
	VIDEO_PREDATA,
	VIDEO_DATA,
	VIDEO_POSTLUDE,
	VIDEO_COMPLETE,
	NUM_VIDSTATES
} vidstate_t;

typedef struct
{
	uint8_t prelude_size;	 // How big is the prelude buffer
	uint8_t postlude_size;	 // How big is the postlude buffer
	uint16_t *databuf;		 // The data buffer
	uint16_t datasize;        // The size of the data buffer
	uint16_t byte;			 // The current byte in a given transfer
	uint8_t command;		 // The current command in a given buffer 
	vidstate_t state;
} vrect_t;

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

static volatile vrect_t vrect;
static uint32_t prelude_buf[32];
static uint32_t postlude_buf[32];
static bool video_send = false;

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

static void video(uint16_t index, uint16_t parameter);
static void video_index(uint16_t val);
static void video_parameter(uint16_t val);
static inline void chipsel_on();
static inline void chipsel_off();
static void video_draw_test_screen();
void video_isr();
static uint8_t find_segments(uint8_t integer);

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
void video_init()
{
  // Set up the Video pins as outputs
  PORT->Group[VIDEO_RST_GROUP].DIRSET.reg = VIDEO_RST;
  PORT->Group[VIDEO_CS_GROUP].DIRSET.reg = VIDEO_CS;

  // Turn the outputs "off"
  PORT->Group[VIDEO_RST_GROUP].OUTSET.reg = VIDEO_RST; // active low
  PORT->Group[VIDEO_CS_GROUP].OUTSET.reg = VIDEO_CS;   // active low

  // Enable the SPI
  spi_init();

  // Wait here for a bit until we know the reset pulse has been big enough.  
  // PAMO-C0201QILK-C Application Note - 3. Power Sequence 
  DelayMs(500);
  PORT->Group[VIDEO_RST_GROUP].OUTCLR.reg = VIDEO_RST; // turn it on 
  DelayMs(20); // Leave it on for a bit. 
  PORT->Group[VIDEO_RST_GROUP].OUTSET.reg = VIDEO_RST; // turn it off
  DelayMs(10);
  video(STAND_BY, 0x0000); // Cancel Standby Mode
  DelayMs(100);
  // The following are specified by page 12 of PAMO-C0201QILK-C.pdf
  video(POWER_CONTROL3, 0x000F);
  video(POWER_CONTROL4, 0x0019);
  DelayMs(32);

  // The following are specified by page 12 of PAMO-C0201QILK-C.pdf

  // Set FP and BP to 8 and the Number of Lines to be 240x224
  // Our display is 176x220. I don't know why they picked 224 here. 
  // I guess it is just the first thing bigger than 220. That said, the 
  // X direction doesn't begin until 0x20 (32) - which, if 
  video(DISPLAY_DUTY, 0x881C); 


  // The RGB Interface is unused - Set DM to 0. 
  video(RGB_INTERFACE, 0x0000);
  // Set the SS bit in the Entry Mode to put 0,0 at the top  left
  // Set up the ID to auto increase the Address Counter after GRAM write
  // Increase X in the window until it hits the edge, then return and increment Y
  video(ENTRY_MODE, 0x0130);
  // Turn off the Display
  video(DISPLAY_CONTROL1, 0x0000); // Turn off display
  //video(STAND_BY, 0x0000);
  // Set the Internal Oscilllator to Max
  video(OSC_REG, 0x0028);
  // The X address starts at 0x20. Why?? No clue.
  // 176 + 32 is 198. So .. that's not it. However, 176 + 64 = 240.  I wonder 
  // if the designer figured on a 240 wide and just slapped te 176 in the middle? 
  // Again - no clue. But - we need to start the X at 0x0020 to get it to show up
  // at the left edge of the screen. 
  video(GRAM_ADDRESS_SET_X, 0x0020); 
  // The Vertical starts at 0 ... that makes sense. 
  video(GRAM_ADDRESS_SET_Y, 0x0000); // Vertical starts at 0.
  // Set up the window to match the entire screen. 
  video(VER_WINDOW_BEG, 0x0000); // Window vertical start (0)
  video(VER_WINDOW_END, 0x00DB); // Window vertical end (219)
  // Horizontal starts at 0x20 and last for 0xAF (176 - 1) - so the end is 0xCF. 
  video(HOR_WINDOW_BEGEND, 0x20CF); 
  
  // Gamma settings from the test doc
  video(GAMMA_TOPBOT_R, 0x3300);
  video(GAMMA_TOPBOT_G, 0x3900);
  video(GAMMA_TOPBOT_B, 0x3800);
  video(GAMMA_R12, 0x2924);
  video(GAMMA_R34, 0x261C);
  video(GAMMA_G12, 0x3125);
  video(GAMMA_G34, 0x271C);
  video(GAMMA_B12, 0x352A);
  video(GAMMA_B34, 0x2A1E);

  //video_draw_test_screen();

  // Turn on the display
  video(DISPLAY_CONTROL1, 0x0001);
  
  // Turn on the interrupt, but not the individual bits.
  NVIC_DisableIRQ(SERCOM4_IRQn);
  NVIC_ClearPendingIRQ(SERCOM4_IRQn);
  NVIC_SetPriority(SERCOM4_IRQn,0);
  NVIC_EnableIRQ(SERCOM4_IRQn);
}

//==============================================================================
void video_set_window(uint8_t x, uint8_t y, uint8_t width, uint8_t height)
{
  uint16_t begend;
  
  // Do the 0x20 shift on X so that we can deal with a base 0 window system. 
  x = x + 0x20; 
  
  // Set up the vertical window
  video(VER_WINDOW_BEG, y); // Window vertical start 
  video(VER_WINDOW_END, y + height -1); // Window vertical end (-1)
  
  // Set up the horizontal window. 
  begend = (x << 8) | (x + width -1); 
  video(HOR_WINDOW_BEGEND, begend);	
}

//==============================================================================
void video_paint_rect(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint16_t color)
{
  uint16_t i;
  video_set_window(x,y,width,height);
  video(GRAM_ADDRESS_SET_X, x + 0x0020);
  video(GRAM_ADDRESS_SET_Y, y); 
  video_index(GRAM_DATA_WRITE);
  chipsel_on();
  spi_write(0x72);
  
  for (i = 0; i < width * height; i++)
  {
	//video_parameter(color);
	spi_write(color >> 8);
	spi_write(color & 0xFF);
  }
  chipsel_off();
  
  video(NO_OP, 0x0000);
}

//==============================================================================
void video_print_string(uint8_t * string, font_t *font, 
                        uint8_t x, uint8_t y, uint16_t fg, uint16_t bg)
{
	//what is the width and height?
	uint16_t width = font->width * strlen(string);	// here it is!
	uint16_t height = font->height;
	uint16_t i, j, k;
	video_set_window(x,y,width,height);
	video(GRAM_ADDRESS_SET_X, x + 0x0020);
	video(GRAM_ADDRESS_SET_Y, y);
	video_index(GRAM_DATA_WRITE);
	chipsel_on();
	spi_write(0x72);
	
	// write out the datas and whatnot
	for(i = 0; i < font->height; i++)
	{
		for(j = 0; j < strlen(string); j++){
			
			uint16_t glyph_row = (string[j] - 32)*height;
			int8_t shift = 0;
			
			// set the "shift" of the character according to the font size
			// also set the glyph_row according to if 12x16 or not
			if(height == 8 && font->width == 6)
			shift = 1;
			if(font->width == 8)
			shift = -1;
			if(height == 16){
				glyph_row = 2*glyph_row + 2*i;
				shift = -5;
			}
			else
			glyph_row = glyph_row + i;
			
			// send out a row of everything
			for(k = 0; k < font->width; k++){
				uint16_t color;
				// set the color to either fg or bg based on the bit in the font
				uint8_t cur;
				// if the font is 12x16 and k is at a certain point, go to next byte
				if(k < 7 && height == 16)
				cur = font->ptr[glyph_row];
				else if(k >= 7 && height == 16){
					cur = font->ptr[glyph_row+1];
					shift = 2;
				}
				else
				cur = font->ptr[glyph_row];	// do the normal thing for everyone else
				
				// check if the pixel is on or off
				if(cur & (1 << ((font->width)-k+shift))) color = fg; else color = bg;
				
				// send out the pixel data
				spi_write(color >> 8);
				spi_write(color & 0xFF);
			}
		}
	}
	chipsel_off();
	
	video(NO_OP, 0x0000);
}

//==============================================================================
void video_print_one_seg(uint16_t integer, uint16_t color, uint8_t x, uint8_t y)
{
	uint8_t ones_digits = find_segments(integer%10);

	// print out segments for hundreds place
	if(ones_digits & (1 << 7)){
		video_paint_rect(4+x,2+y,10,2,color);	// a seg
	}
	else{
		video_paint_rect(4+x,2+y,10,2,0x0000);
	}
	if(ones_digits & (1 << 6)){
		video_paint_rect(14+x,6+y,2,10,color);	// b seg
		}else{
		video_paint_rect(14+x,6+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 5)){
		video_paint_rect(14+x,22+y,2,10,color);	// c seg
		}else{
		video_paint_rect(14+x,22+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 4)){
		video_paint_rect(4+x,34+y,10,2,color);	// d seg
		}else{
		video_paint_rect(4+x,34+y,10,2,0x0000);
	}
	if(ones_digits & (1 << 3)){
		video_paint_rect(2+x,22+y,2,10,color);	// e seg
		}else{
		video_paint_rect(2+x,22+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 2)){
		video_paint_rect(2+x,6+y,2,10,color);	// f seg
		}else{
		video_paint_rect(2+x,6+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 1)){
		video_paint_rect(4+x,18+y,10,2,color);	// g seg
		}else{
		video_paint_rect(4+x,18+y,10,2,0x0000);
	}
}

//==============================================================================
void video_print_three_segs(uint16_t integer, uint16_t color, uint8_t x, uint8_t y)
{
	uint8_t tens_digits;
	uint8_t ones_digits;
	uint8_t hundreds_digits;
	
	hundreds_digits = find_segments((integer/100)%10);
	tens_digits = find_segments((integer/10)%10);
	ones_digits = find_segments(integer%10);
	
	// print out segments for hundreds place
	if(hundreds_digits & (1 << 7)){
		video_paint_rect(4+x,2+y,10,2,color);	// a seg
	}
	else{
		video_paint_rect(4+x,2+y,10,2,0x0000);
	}
	if(hundreds_digits & (1 << 6)){
		video_paint_rect(14+x,6+y,2,10,color);	// b seg
		}else{
		video_paint_rect(14+x,6+y,2,10,0x0000);
	}
	if(hundreds_digits & (1 << 5)){
		video_paint_rect(14+x,22+y,2,10,color);	// c seg
		}else{
		video_paint_rect(14+x,22+y,2,10,0x0000);
	}
	if(hundreds_digits & (1 << 4)){
		video_paint_rect(4+x,34+y,10,2,color);	// d seg
		}else{
		video_paint_rect(4+x,34+y,10,2,0x0000);
	}
	if(hundreds_digits & (1 << 3)){
		video_paint_rect(2+x,22+y,2,10,color);	// e seg
		}else{
		video_paint_rect(2+x,22+y,2,10,0x0000);
	}
	if(hundreds_digits & (1 << 2)){
		video_paint_rect(2+x,6+y,2,10,color);	// f seg
		}else{
		video_paint_rect(2+x,6+y,2,10,0x0000);
	}
	if(hundreds_digits & (1 << 1)){
		video_paint_rect(4+x,18+y,10,2,color);	// g seg
		}else{
		video_paint_rect(4+x,18+y,10,2,0x0000);
	}
	
	uint8_t shift = 17 + x;
	// print out segments for tens place
	if(tens_digits & (1 << 7)){
		video_paint_rect(4+shift,2+y,10,2,color);	// a seg
	}
	else{
		video_paint_rect(4+shift,2+y,10,2,0x0000);
	}
	if(tens_digits & (1 << 6)){
		video_paint_rect(14+shift,6+y,2,10,color);	// b seg
		}else{
		video_paint_rect(14+shift,6+y,2,10,0x0000);
	}
	if(tens_digits & (1 << 5)){
		video_paint_rect(14+shift,22+y,2,10,color);	// c seg
		}else{
		video_paint_rect(14+shift,22+y,2,10,0x0000);
	}
	if(tens_digits & (1 << 4)){
		video_paint_rect(4+shift,34+y,10,2,color);	// d seg
		}else{
		video_paint_rect(4+shift,34+y,10,2,0x0000);
	}
	if(tens_digits & (1 << 3)){
		video_paint_rect(2+shift,22+y,2,10,color);	// e seg
		}else{
		video_paint_rect(2+shift,22+y,2,10,0x0000);
	}
	if(tens_digits & (1 << 2)){
		video_paint_rect(2+shift,6+y,2,10,color);	// f seg
		}else{
		video_paint_rect(2+shift,6+y,2,10,0x0000);
	}
	if(tens_digits & (1 << 1)){
		video_paint_rect(4+shift,18+y,10,2,color);	// g seg
		}else{
		video_paint_rect(4+shift,18+y,10,2,0x0000);
	}
	
	shift += 17;
	// print out segments for ones place
	if(ones_digits & (1 << 7)){
		video_paint_rect(4+shift,2+y,10,2,color);	// a seg
	}
	else{
		video_paint_rect(4+shift,2+y,10,2,0x0000);
	}
	if(ones_digits & (1 << 6)){
		video_paint_rect(14+shift,6+y,2,10,color);	// b seg
		}else{
		video_paint_rect(14+shift,6+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 5)){
		video_paint_rect(14+shift,22+y,2,10,color);	// c seg
		}else{
		video_paint_rect(14+shift,22+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 4)){
		video_paint_rect(4+shift,34+y,10,2,color);	// d seg
		}else{
		video_paint_rect(4+shift,34+y,10,2,0x0000);
	}
	if(ones_digits & (1 << 3)){
		video_paint_rect(2+shift,22+y,2,10,color);	// e seg
		}else{
		video_paint_rect(2+shift,22+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 2)){
		video_paint_rect(2+shift,6+y,2,10,color);	// f seg
		}else{
		video_paint_rect(2+shift,6+y,2,10,0x0000);
	}
	if(ones_digits & (1 << 1)){
		video_paint_rect(4+shift,18+y,10,2,color);	// g seg
		}else{
		video_paint_rect(4+shift,18+y,10,2,0x0000);
	}
}

//==============================================================================
bool video_submit_rect(uint8_t x, uint8_t y, uint8_t width, uint8_t height, 
                       uint16_t *buf)
{
	uint8_t command = 0;

	// if we can't have it, blow it all up
	if(spi_lock(SPI_LOCK_VIDEO) != SPI_LOCK_VIDEO || video_send == true)
		return false;
	
	// Turn on interrupts
	SERCOM4->SPI.INTENSET.bit.TXC = 1;


	// indent the x to match the screen
	x = x + 0x20;
	
	// Set up the vrect
	vrect.databuf = buf;
	vrect.datasize = (uint16_t) width * height * 2; // 2 bytes per pixel
	vrect.byte = 0;
	vrect.command = 0;
	vrect.state = VIDEO_PRELUDE;
	
	// Set up the prelude buffer;
	prelude_buf[command++] = VID(VER_WINDOW_BEG, (uint16_t) y);
	prelude_buf[command++] = VID(VER_WINDOW_END, (uint16_t) y + height -1);
    prelude_buf[command++] = VID(HOR_WINDOW_BEGEND, (x << 8) | (x + width -1));
	prelude_buf[command++] = VID(GRAM_ADDRESS_SET_X, x );
	prelude_buf[command++] = VID(GRAM_ADDRESS_SET_Y, y);
	prelude_buf[command++] = VID_INDEX(GRAM_DATA_WRITE);
	vrect.prelude_size = command;

	
	// Set up the postlude buffer
	command = 0;
	postlude_buf[command++] = VID(NO_OP, 0x0000);
	vrect.postlude_size = command;
	
 
	// Send the first byte
	vrect.byte ++;	
	spi_register_isr(video_isr);
	chipsel_on();
	spi_write_interrupt(0x70);
	video_send = true;
	//SERCOM4->SPI.DATA.bit.DATA = 0x70;
	return true;
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//==============================================================================
static void video(uint16_t index, uint16_t parameter)
{
  video_index(index);
  video_parameter(parameter);
}

//==============================================================================
static void video_index(uint16_t val)
{
  chipsel_on();
  spi_write(0x70);
  spi_write(0x00);
  spi_write(val & 0x00FF);
  chipsel_off();
}

//==============================================================================
static void video_parameter(uint16_t val)
{
  chipsel_on();
  spi_write(0x72);
  spi_write(val >> 8);
  spi_write(val & 0xFF);
  chipsel_off();
}

//==============================================================================
static void video_draw_test_screen()
{
  uint16_t i,j;
  uint16_t color;

  video_index(GRAM_DATA_WRITE);

  for (i = 0; i < 176*220; i++)
  {
    color = 0x7BEF;
    if (i >= 176*20) color = 0xF800; // Red
    if (i >= 176*40) color = 0x07E0; // Green
    if (i >= 176*60) color = 0x001F; // Blue
    if (i >= 176*80) color = 0x0000; // Black
    if (i >= 176*100) color = 0xFFFF; // White
    if (i >= 176*120) color = 0xF81F; // Magenta
    if (i >= 176*140) color = 0xFFE0; // Yellow
    if (i >= 176*160) color = 0x07FF; // Cyan
    if (i >= 176*180) color = 0xF3E0; // Orange
    if (i >= 176*200) color = 0x781F; // Purple

    video_parameter(color);
  }
  // Write address 0 to stop the cycle
  video(NO_OP, 0x0000);

  // Let's draw a square in the middle of the screen. Make it RED.
  color = 0xF800;
  for (i = 20; i < 120; i++)
  {
    video(GRAM_ADDRESS_SET_X, (uint16_t) (0x0020 + 10));
    video(GRAM_ADDRESS_SET_Y, (uint16_t) (i));
    video_index(GRAM_DATA_WRITE);

    for (j = 0; j < 10; j++)
    {
      video_parameter(color);
    }
    video(NO_OP, 0x0000);
  }
  
  
}

//==============================================================================
static void inline chipsel_on()
{
  PORT->Group[VIDEO_CS_GROUP].OUTCLR.reg = VIDEO_CS; // active low
}

//==============================================================================
static void inline chipsel_off()
{
  PORT->Group[VIDEO_CS_GROUP].OUTSET.reg = VIDEO_CS; // active low
}

//==============================================================================
static uint8_t find_segments(uint8_t integer)
{
	uint8_t retval = 0;
	// figure out the tens segments
	switch(integer){
		case 0:
		retval = 0b11111100;
		break;
		case 1:
		retval = 0b01100000;
		break;
		case 2:
		retval = 0b11011010;
		break;
		case 3:
		retval = 0b11110010;
		break;
		case 4:
		retval = 0b01100110;
		break;
		case 5:
		retval = 0b10110110;
		break;
		case 6:
		retval = 0b10111110;
		break;
		case 7:
		retval = 0b11100000;
		break;
		case 8:
		retval = 0b11111110;
		break;
		case 9:
		retval = 0b11110110;
		break;
		default:
		retval = 0;
		break;
	}
	return retval;
}

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`   
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------

//==============================================================================
void video_isr()
{
	uint8_t function;
	
	uint32_t *command_buf = prelude_buf; // Assume prelude
	uint8_t bufsize = vrect.prelude_size;
	
	switch (vrect.state)
	{
		// If we are doing a postlude, switch the command buf to prelude
		case VIDEO_POSTLUDE:
		{
			command_buf = postlude_buf;
			bufsize = vrect.postlude_size;
			// Keep going - don't break here ... 
		} 
			
		case VIDEO_PRELUDE:
		{
			// Extract the command portion
			function = command_buf[vrect.command] >> 24;

			switch (function)
			{
				case VIDCMD_VIDEO:
				{
					switch (vrect.byte)
					{
						case 0:
						{
							chipsel_off(); // Turn off any prior chip select
							chipsel_on();
							spi_write_interrupt(0x70);
							vrect.byte ++;
						}
						break;
						case 1:
						{
							spi_write_interrupt(0x00);
							vrect.byte ++;
						}
						break;
						case 2:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 16) & 0xFF);
							vrect.byte ++;
						}
						break;
						case 3:
						{
							chipsel_off();
							chipsel_on();
							spi_write_interrupt(0x72);
							vrect.byte ++;
						}
						break;
						case 4:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 8) & 0xFF);
							vrect.byte ++;
						}
						break;
						case 5:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 0) & 0xFF);
							vrect.byte = 0;
							vrect.command ++;
						}
						break;
					} // switch vrect.byte
				} // case VIDCMD_VIDEO
				break;				
				case VIDCMD_INDEX:
				{
					switch (vrect.byte)
					{
						case 0:
						{
							chipsel_off(); // Turn off any prior chip select
							chipsel_on();
							spi_write_interrupt(0x70);
							vrect.byte ++;
						}
						break;
						case 1:
						{
							spi_write_interrupt(0x00);
							vrect.byte ++;
						}
						break;
						case 2:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 16) & 0xFF);
							vrect.byte = 0;
							vrect.command ++;
						}
					} // switch vrect.byte
				} // case VIDCMD_INDEX
				break;
				
				case  VIDCMD_PARAM:
				{
					switch (vrect.byte)
					{						
						case 0:
						{
							chipsel_off(); // Turn off any prior chip select
							chipsel_on();
							spi_write_interrupt(0x72);
							vrect.byte ++;
						}
						break;
						case 1:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 8) & 0xFF);
							vrect.byte ++;
						}
						break;
						case 2:
						{
							spi_write_interrupt((command_buf[vrect.command] >> 0) & 0xFF);
							vrect.byte = 0;
							vrect.command ++;
						}
						break;	
					} // switch vrect.byte	
				} //case VIDCMD_PARAM
				break;
			} // switch command
			
			// See if we are finished with the command transfer
			if (vrect.command == bufsize)
			{
				if (vrect.state == VIDEO_PRELUDE)
				{
					vrect.state = VIDEO_PREDATA;
					vrect.byte = 0;
					vrect.command = 0;
				}
				else
				{
					vrect.state = VIDEO_COMPLETE;
				}
			}
		} // case VIDEO_PRELUDE
		break;
		
		case VIDEO_PREDATA:
		{
			// Turn on the chipsel, select the param register (0x72), and head to the data
			chipsel_off(); // Turn off any prior chip select
			chipsel_on();
			spi_write_interrupt(0x72);
			vrect.state = VIDEO_DATA;
		} // case VIDEO_PREDATA
		break;
		
		case VIDEO_DATA:
		{
			uint16_t data;
            data = vrect.databuf[vrect.byte/2];
			if (vrect.byte%2 == 0) // first (hight) byte
			{
				data = data >> 8 & 0xFF;
			}
			else // second (low) byte
			{
				data = data & 0xFF;
			}
			// Send the data byte
			spi_write_interrupt(data);
			vrect.byte++;
			// See if we are finished
			if (vrect.byte == vrect.datasize)
			{
				vrect.state = VIDEO_POSTLUDE;
				vrect.byte = 0;
				vrect.command = 0;
			}

			
		} // case VIDEO_DATA
		break;
		
		case VIDEO_COMPLETE:
		{
			chipsel_off();
			// Turn off the interrupts
			SERCOM4->SPI.INTENCLR.bit.TXC = 1;
			video_send = false;
			spi_unlock();
		} // case VIDEO_COMPLETE
		break;		
		
		default:
		{
			while(1); // This shouldn't happen
		}
	} // switch vrect.state
}
