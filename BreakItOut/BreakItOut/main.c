/*****************************************************************
*
* Name : Daniel Floratos
* Program : Program The Last
* Class : ENGE 320
* Date : 12/8/19
* Description : Breakout
*
* Intro screen has name: (10) ____ X
* Intro screen has unlimited/limited mode: (10) ____ X
* Intro screen has controller options: (10) ____ X
* Intro screen selects options using joystick and toggles options with button presses: (10) ____ X
* Intro screen action is smooth (easy to operate): (10) ____ X
* Game screen prints lives: (10) ____ X
* Game screen updates lives while playing: (10) ____ X
* Game screen prints high score, score: (10) ____ X
* Game screen prints score: (10) ____ X
* Game screen updates score while playing: (10) ____ X
* High score is maintained between games: (10) ____ X
* Ball bounces properly off of wall/bricks: (10) ____ X
* Ball bounces variably off of paddle varying direction and speed in at least 7 regions: (30) ____ X
* Ball only erases one brick for each hit: (10) ____ X
* Paddle moves correctly in horizontal: (10) ____ X
* Paddle moves correctly in vertical: (10) ____ X
* Paddle moves correctly in angle: (20) ____ X
* Joystick can be used as input controller: (10) ____ X
* Accelerometer can be used as input controller: (10) ____ 
* LED�s track input in limited mode: (10) ____ X
* LED�s track input in unlimited mode: (10) ____ X 
* LED�s use a smooth fade that isn�t interrupted by the video: (20) ____ X
* Sounds are emitted at the appropriate time and don�t affect game play (and vice versa): (30) ____ X
* Sounds are varied for different elements: (10) ____ X
* Game continues play faster when bricks are clear: (20) ____ X
* Game plays demo mode after delay: (10) ____ X
* Demo mode plays game properly: (30) ____ X
* Demo mode doesn�t lock in boring loop: (10) ____ X
* Game lives operate correctly: (10) ____ X
* Video is smooth and not interrupted by LEDs (or anything else): (20) ____ X
*
*****************************************************************/

//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "sam.h"
#include "adc.h"
#include "accelerometer.h"
#include "tlc5940.h"
#include "video.h"
#include "timer.h"
#include "speaker.h"
#include "buttons.h"
#include "eeprom.h"
#include "delay.h"
#include <stdbool.h>
#include <math.h>

//-----------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//-----------------------------------------------------------------------------

#define BUFSIZE (256)
#define SEG_COLOR (0xFFFF)
#define SCORE_OFFSET (124)
#define LIVES_OFFSET (80)
#define PADDLE_BEEP (200)
#define WALL_BEEP (300)
#define BLOCK_BEEP (400)
#define DEAD_BEEP (100)

//-----------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//-----------------------------------------------------------------------------

typedef struct{
	float x;
	float y;
	uint8_t width;
	uint8_t height;
	bool exists;
	bool needWrite;
	uint8_t points;
	float note;
} Block;

typedef struct{
	float x;
	float y;
	uint8_t width;
	uint8_t height;
	float velocity_x;
	float velocity_y;
	bool needWrite;
} Ball;

typedef enum{
	BALL_TIMER,
	BEEP_TIMER,
	DEMO_TIMER,
	NUM_TIMERS	
} Timers;

typedef enum{
	INTRO_STATE,
	INPUT_STATE,
	UNLIMITED_STATE,
	LIMITED_STATE,
	DEATH_STATE,
	LOSE_STATE,
	NUM_STATES
} GameStates;

typedef struct{
	GameStates current;
	uint16_t score;
	uint16_t highscore;
	uint8_t lives;
	bool newScore;
} Game;

//-----------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//-----------------------------------------------------------------------------

static Block blocks[72];
static Game game;
static uint16_t blackBuf[BUFSIZE]; // Doing it here to avoid dynamic allocation on the stack
static uint16_t whiteBuf[BUFSIZE]; // Doing it here to avoid dynamic allocation on the stack
static uint64_t timers[NUM_TIMERS];
static uint16_t leds[16];
static Ball ball;
static Block paddle;
double beep = 0;
bool winFlag = false;
bool ledUpdate = false;

font_t* myfont;
font_t* welcomeFont;

double LEDs[16];
uint16_t LEDpwm[16];
GameStates gameMode = LIMITED_STATE;
bool joyLock = false;
uint8_t choice = 0;
bool joystick = true;
bool inputMethod = true;
bool inDemo = false;
uint8_t demoOffset = 50;

//-----------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//-----------------------------------------------------------------------------

void initialize_board(float xVel, float yVel, bool respawn);
void clear_block(Block *deadblock);
bool isCollide(Block theBlock);
void bounceBall(Ball *ball, uint8_t direction, float oldX, float oldY);
uint8_t findBounce(Block theBlock);
uint8_t wallBounce(float oldX, float oldY);
void gameStuff(float inputX, float inputY, float inputMag, bool demo);
void increaseSpeed();
void mode23_func(float magnitude, float x, float y);
void double_toInt(uint16_t intArr[], double doubleArr[]);
void bouncePaddle(float oldX, float oldY, uint8_t bounce);
void introduceYoself();
void chooseMode(float input);
bool chooseInput(float input);

//-----------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//-----------------------------------------------------------------------------

//=============================================================================
int main(void)
{
    /* Initialize so much stuff */
    SystemInit();
	i2c_init();
	accelerometer_init();
	adc_init();
	tlc_init();
	video_init();
	timer_init();
	speaker_init();
	buttons_init();
	
	myfont = font_get(FONT_8x12);
	welcomeFont = font_get(FONT_12x16);
	
	// initialize leds
	for(int i = 0; i < 16; i++){
		leds[i] = 0;
	}
	tlc_write(leds);
	while(spi_isLocked() == true){}
	
	// initialize the buffers for writing and clearing
	for (int i = 0; i < BUFSIZE; i++)
	{
		whiteBuf[i] = 0xFFFF;
		blackBuf[i] = 0x0000;
	}
	
	// initialize the timers
	for (int i = 0; i < NUM_TIMERS; i++)
	{
		timers[i] = 0;
	}
	
	// read in the high score
	uint8_t read_data[2];
	//eeprom_read(0x0FAB, read_data, 2);
	
	// initialize game score and high score
	game.current = LOSE_STATE;
	//game.highscore = ((read_data[0] << 8) | read_data[1]);
	game.highscore = 0;
	game.score = 0;
	game.lives = 5;
	game.newScore = false;
	
	// initialize ball
	ball.x = 29;
	ball.y = 139;
	ball.width = 4;
	ball.height = 4;
		
	// set up some static variables for the main loop
	static double x;
	static double y;
	static double mag;
	
	void* seed = read_data;
	srand(seed);
		
    /* this is my application code */
    while (1) 
    {	
		// update the accelerometer (DOES NOT PLAY WELL WITH SPI)
		//if(!spi_isLocked()){
			//accelerometer_update();
		//}
		
		// refresh the speaker
		speaker_out(beep);
		
		// get input, be it joystick or accelerometer
		if(joystick){
			x = findModifier(XAXIS);
			y = findModifier(YAXIS);
			mag = findMagnitude();
		}
		else if(!joystick){
			//x = accelerometer_findModifier(XAXIS);	// accel doesn't like the spi
			//y = accelerometer_findModifier(YAXIS);
		}
		
		switch(game.current){
			case INTRO_STATE:
				chooseMode(y);
			break;
			case INPUT_STATE:
			break;
			case LIMITED_STATE:
				// play game in limited mode
				gameStuff(x, 0, mag, false);
			break;
			case UNLIMITED_STATE:
				// play game in unlimited mode
				gameStuff(x, y, mag, false);
			break;
			case DEATH_STATE:
				if(timer_get() - timers[BEEP_TIMER] > 1000){
					game.current = LOSE_STATE;
					beep = 0;
					speaker_out(beep);
				}
			break;
			case LOSE_STATE:
				game.lives = 5;
				initialize_board(2,2, false);
				if(!inDemo){
					introduceYoself();
					timers[DEMO_TIMER] = timer_get();
					ball.x = 29;
					ball.y = 139;
					choice = 0;
				}
				game.current = INTRO_STATE;
			break;
		}
    }
}

//-----------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//-----------------------------------------------------------------------------

//=============================================================================
void introduceYoself()
{
	video_print_string("BREAKOUT",welcomeFont,40,110,0xFFFF,0x0000);
	video_print_string("Limited Mode",myfont,40,135,0xFFFF,0x0000);
	video_print_string("Unlimited Mode",myfont,40,135+16,0xFFFF,0x0000);
	video_print_string("Joystick Input",myfont,40,135+32,0xFFFF,0xFD80);
	video_print_string("Accel. Input",myfont,40,135+48,0xFFFF,0x38E7);
	video_paint_rect(29,139,ball.width,ball.height,0xFFFF);
}

//=============================================================================
void chooseMode(float input)
{	
	if(input >= 0.99 && !joyLock && !inDemo){
		joyLock = true;	// prevent extra moves
		video_paint_rect(ball.x,ball.y,ball.width,ball.height,0x0000);	// clear ball
		if(choice > 0)	// move ball
			choice--;
		ball.y = 139+16*choice;
		video_paint_rect(ball.x,ball.y,ball.width,ball.height,0xFFFF);	// draw new ball
		timers[DEMO_TIMER] = timer_get();	// reset demo timer
	}
	else if(input <= -0.99 && !joyLock && !inDemo){
		joyLock = true;	// prevent extra moves
		video_paint_rect(ball.x,ball.y,ball.width,ball.height,0x0000);	// clear ball
		if(choice < 3)	// move ball
			choice++;
		ball.y = 139+16*choice;
		video_paint_rect(ball.x,ball.y,ball.width,ball.height,0xFFFF);	// draw new ball
		timers[DEMO_TIMER] = timer_get();	// reset demo timer
	}
	else if(input <= 0.9 && input >= -0.9){	// unlock joystick movement
		joyLock = false;
	}
	
	if(buttons_get(0) || buttons_get(1) || buttons_get(2) || buttons_get(3)){
		// wait for buttons to be released
		while(buttons_get(0) || buttons_get(1) || buttons_get(2) || buttons_get(3)){}
			
		if(inDemo){	// get us out of demo mode
			inDemo = false;
			speaker_out(0);
			game.score = 0;
			initialize_board(0,0,false);
			introduceYoself();
			beep = 0;
			timers[DEMO_TIMER] = timer_get();
			ball.x = 29;
			ball.y = 139;
			inputMethod = true;
			return;
		}
			
		switch (choice)
		{
			case 0:	// start in limited mode
				game.current = LIMITED_STATE;
				joystick = inputMethod;
				initialize_board(2,2, false);
			break;
			case 1:	// start in unlimited mode
				game.current = UNLIMITED_STATE;
				joystick = inputMethod;
				initialize_board(2,2, false);
			break;
			case 2:	// set input to joystick
				video_print_string("Joystick Input",myfont,40,135+32,0xFFFF,0xFD80);
				video_print_string("Accel. Input",myfont,40,135+48,0xFFFF,0x38E7);
				inputMethod = true;
			break;
			case 3:	// set input to accelerometer
				video_print_string("Joystick Input",myfont,40,135+32,0xFFFF,0x38E7);
				video_print_string("Accel. Input",myfont,40,135+48,0xFFFF,0xFD80);
				inputMethod = false;
			break;
		}
		timers[DEMO_TIMER] = timer_get();	// reset demo timer
	}
	
	// start the demo stuff
	if(timer_get() - timers[DEMO_TIMER] > 5000 && !inDemo){
		initialize_board(2,2,false);
		inDemo = true;
	}
	else if(inDemo){
		gameStuff(0,0,0,true);	// play the demo stuff
	}
}

//=============================================================================
void gameStuff(float inputX, float inputY, float inputMag, bool demo)
{
	// update the ball every 16 millis
	if(timer_get() - timers[BALL_TIMER] > 16 && ball.needWrite == false){
		// clear ball
		while(spi_isLocked() ==  true){}
		video_submit_rect(ball.x,ball.y,ball.width,ball.height,blackBuf);
		ball.needWrite = true;	// queue a write
		
		// clear paddle
		while(spi_isLocked() ==  true){}
		video_submit_rect(paddle.x,paddle.y,paddle.width,paddle.height,blackBuf);
		paddle.needWrite = true;	// queue a write
		
		float oldBallX = ball.x;	// save the old locations
		float oldBallY = ball.y;
		
		uint8_t numChecks = 3;
		for(int i = 0; i < numChecks; i++){
			
			ball.x += ball.velocity_x/numChecks;	// move the ball
			ball.y -= ball.velocity_y/numChecks;	// y is inverted because screen is silly
			
			// check for paddle collision
			if(isCollide(paddle)){
				uint8_t bounceDir = findBounce(paddle);
				bouncePaddle(oldBallX, oldBallY, bounceDir);
				beep = paddle.note;	// do a beep
				timers[BEEP_TIMER] = timer_get();
				break;
			}
			
			if(!demo){
				paddle.x += inputX;	// move according to joysticks
				paddle.y -= inputY;
			}
			else{	// move in somewhat random pattern for demo mode
				uint8_t optimalPos = ball.x-paddle.width/2 + 10*(demoOffset-50)/50.0;
				if(optimalPos > 220){
					optimalPos = 0;
				}
				
				else if(optimalPos - paddle.x > 1){
					paddle.x += 1;
				}
				else if(optimalPos - paddle.x < -1){
					paddle.x -= 1;
				}
			}
			
			// make sure the paddle doesn't go out of bounds
			if(paddle.x <= 10 || paddle.x > 200)
				paddle.x = 10;
			else if(paddle.x >= (166-paddle.width) && paddle.x <= 200)
				paddle.x = 166-paddle.width;

			if(paddle.y < 135)
				paddle.y = 135;
			else if(paddle.y > (220-paddle.height))
				paddle.y = 220 - paddle.height;
			
			// check for paddle collision again
			if(isCollide(paddle)){
				uint8_t bounceDir = findBounce(paddle);
				bouncePaddle(oldBallX, oldBallY, bounceDir);
				beep = paddle.note;		// do a beep
				timers[BEEP_TIMER] = timer_get();
				break;
			}
			
			// see if we collided with anyone
			uint8_t collider = 0xFF;
			for(int i = 0; i < 72; i++){
				if(isCollide(blocks[i])){
					collider = i;
				}
			}	
			
			// if a collision was detected, do things
			if(collider < 0xFF){
				uint8_t bounceDir = findBounce(blocks[collider]);
				bounceBall(&ball, bounceDir, oldBallX, oldBallY);
				demoOffset = rand()%100;
				
				// delete the deadbois and get the points
				clear_block(&blocks[collider]);
				game.score += blocks[collider].points;
				game.newScore = true;
				
				beep = blocks[collider].note; // do a beep
				timers[BEEP_TIMER] = timer_get();
				break;
			}	
		}
		
		winFlag = true;
		for(int i = 0; i < 72; i++){
			if(blocks[i].exists){
				winFlag = false;
				break;
			}
		}
		
		if(winFlag){
			// do win things
			increaseSpeed();
			speaker_out(600);
			initialize_board(ball.velocity_x,ball.velocity_y,false);
			speaker_out(0);
			winFlag = false;
		}
	
		uint8_t bouncey = wallBounce(oldBallX, oldBallY);
	
		if(bouncey == 1){
			beep = WALL_BEEP;
			demoOffset = rand()%100;
			timers[BEEP_TIMER] = timer_get();
		}
		else if(bouncey == 0){
			srand(rand());
			return;
		}
	
		// re-write the ball and paddle
		if(ball.needWrite){
			while(spi_isLocked() == true){}
			video_submit_rect(ball.x,ball.y,ball.width,ball.height,whiteBuf);
			ball.needWrite = false;
		}
		
		if(paddle.needWrite){
			while(spi_isLocked() == true){}
			video_submit_rect(paddle.x,paddle.y,paddle.width,paddle.height,whiteBuf);
			paddle.needWrite = false;
		}
		
		// call an LED update
		ledUpdate = true;
					
		// reset the draw timer
		timers[BALL_TIMER] = timer_get();
	}
	
	if(!spi_isLocked() && game.newScore){
		video_print_three_segs(game.score, SEG_COLOR, SCORE_OFFSET, 0);
		game.newScore = false;
	}
	
	// update the LEDs if they need it
	if(!spi_isLocked() && ledUpdate){
		mode23_func(inputMag, inputX, inputY);
		ledUpdate = false;
	}
	
	// update the sound if it needs it
	if((timer_get() - timers[BEEP_TIMER]) > 200){
		beep = 0;
	}
}

//=============================================================================
void initialize_board(float xVel, float yVel, bool respawn)
{	
	// initialize the ball
	ball.x = 86;
	ball.y = 180;
	ball.width = 4;
	ball.height = 4; 
	ball.velocity_x = xVel;
	ball.velocity_y = yVel;
	ball.needWrite = false;
	
	// initialize the paddle
	paddle.x = 74;
	paddle.y = 190;
	paddle.width = 28;
	paddle.height = 5;
	paddle.exists = true;
	paddle.needWrite = false;
	paddle.note = PADDLE_BEEP;
	
	if(!respawn){
		// write border
		video_paint_rect(0,0,176,220,0xFFFF);
		video_paint_rect(0,0,176,40,0x0000);
		video_paint_rect(10,50,156,170,0x0000);
		
		// draw blocks (blocks are 13 by 5)
		video_paint_rect(10,70,156,5,0xF800);
		video_paint_rect(10,75,156,5,0xFD80);
		video_paint_rect(10,80,156,5,0xFFE0);
		video_paint_rect(10,85,156,5,0x07E0);
		video_paint_rect(10,90,156,5,0x07BF);
		video_paint_rect(10,95,156,5,0x001F);
		
		// initialize bricks
		for(int i = 0; i < 72; i++){
			blocks[i].x = 10 + 13*(i%12);
			blocks[i].y = 70 + 5*(i/12);
			blocks[i].note = 525 - 25*(i/12);
			blocks[i].width = 13;
			blocks[i].height = 5;
			blocks[i].exists = true;
			blocks[i].needWrite = false;
			
			if(i < 24){
				blocks[i].points = 7;
			}
			else if(i < 48){
				blocks[i].points = 4;
			}
			else if(i < 72)
				blocks[i].points = 1;
		}
	}
	
	// print the score
	video_print_three_segs(game.score, SEG_COLOR, SCORE_OFFSET, 0);
	
	// print the lives
	video_print_one_seg(game.lives, SEG_COLOR, LIVES_OFFSET, 0);
	
	// draw the highscore
	video_print_three_segs(game.highscore, SEG_COLOR, 0, 0);
}

//=============================================================================
void clear_block(Block *deadblock)
{
	// gets the values from the dead block
	uint8_t x = deadblock->x;
	uint8_t y = deadblock->y;
	uint8_t width = deadblock->width;
	uint8_t height = deadblock->height;
	
	// write black over the block
	while(spi_isLocked() == true){}
	video_submit_rect(x,y,width,height,blackBuf);
	
	// tell everyone else it's dead
	deadblock->exists = false;
}

//=============================================================================
bool isCollide(Block theBlock)
{
	
	// returns false if the block is dead
	if(!theBlock.exists){
		return false;	
	}
	
	// checks for collision and returns true if it is colliding.
	if((ball.x >= theBlock.x-ball.width && ball.x <= theBlock.x+theBlock.width)
		&& (ball.y >= theBlock.y-ball.height && ball.y <= theBlock.y+theBlock.height)){
		
		return true;
	}
	// catch-all return false
	return false;
}

//=============================================================================
uint8_t findBounce(Block theBlock)
{
	uint8_t bounceX = 0;
	uint8_t bounceY = 0;
	
	// 2 is left and 1 is right
	if(ball.x <= theBlock.x-ball.width+(theBlock.width/11) 
		&& ball.x >= theBlock.x-ball.width)	
	{
		bounceX = 2;
	}
	else if(ball.x >= theBlock.x+(theBlock.width/1.1) 
			&& ball.x <= theBlock.x+theBlock.width)
	{
		bounceX = 1;
	}
	
	// 2 is down and 1 is up
	if(ball.y >= theBlock.y-ball.height
		&& ball.y <= theBlock.y-ball.height+(theBlock.height/3))
	{
		bounceY = 1;
	}
	else if(ball.y <= theBlock.y+theBlock.height
			&& ball.y >= theBlock.y+(theBlock.height/1.5))
	{
		bounceY = 2;
	}
	
	// encoding the possible bounce directions
	return 3*bounceX + bounceY;
}

//=============================================================================
void bounceBall(Ball *ball, uint8_t direction, float oldX, float oldY)
{
	switch(direction){
		case 0:
		break;
		case 1:
			if(ball->velocity_y < 0)
				ball->velocity_y = -ball->velocity_y;
			ball->y -= ball->velocity_y;
		break;
		case 2:
			if(ball->velocity_y > 0)
				ball->velocity_y = -ball->velocity_y;
			ball->y -= ball->velocity_y;
		break;
		case 3:
			if(ball->velocity_x < 0)
				ball->velocity_x = -ball->velocity_x;
			ball->x += ball->velocity_x;
		break;
		case 4:
			if(ball->velocity_x < 0)
				ball->velocity_x = -ball->velocity_x;
			if(ball->velocity_y < 0)
				ball->velocity_y = -ball->velocity_y;
			ball->x += ball->velocity_x;
			ball->y -= ball->velocity_y;
		break;
		case 5:
			if(ball->velocity_x < 0)
				ball->velocity_x = -ball->velocity_x;
			if(ball->velocity_y > 0)
				ball->velocity_y = -ball->velocity_y;
			ball->x += ball->velocity_x;
			ball->y -= ball->velocity_y;
		break;
		case 6:
			if(ball->velocity_x > 0)
				ball->velocity_x = -ball->velocity_x;
			ball->x += ball->velocity_x;
		break;
		case 7:
			if(ball->velocity_x > 0)
				ball->velocity_x = -ball->velocity_x;
			if(ball->velocity_y < 0)
				ball->velocity_y = -ball->velocity_y;
			ball->x += ball->velocity_x;
			ball->y -= ball->velocity_y;
		break;
		case 8:
			if(ball->velocity_x > 0)
				ball->velocity_x = -ball->velocity_x;
			if(ball->velocity_y > 0)
				ball->velocity_y = -ball->velocity_y;
			ball->x += ball->velocity_x;
			ball->y -= ball->velocity_y;
		break;
	}
}

//=============================================================================
void bouncePaddle(float oldX, float oldY, uint8_t bounce)
{
	switch(bounce){
		case 0:
		break;
		case 1:
			if(ball.velocity_y < 0)
				ball.velocity_y = -ball.velocity_y;
				
			float mag = sqrt(ball.velocity_x*ball.velocity_x + ball.velocity_y*ball.velocity_y);
			float dif = ball.x - paddle.x;
			
			if(dif < 4){
				ball.velocity_x = -mag*cos(0.78539816);
				ball.velocity_y = mag*sin(0.78539816);
			}
			//else if(dif < 4){
				//
			//}
			else if(dif < 8){
				ball.velocity_x = -mag*cos(1.04719755);
				ball.velocity_y = mag*sin(1.04719755);
			}
			else if(dif < 12){
				ball.velocity_x = -mag*cos(1.30899694);
				ball.velocity_y = mag*sin(1.30899694);
			}
			else if(dif < 16){
				ball.velocity_x = 0;
				ball.velocity_y = mag;
			}
			else if(dif < 20){
				ball.velocity_x = mag*cos(1.30899694);
				ball.velocity_y = mag*sin(1.30899694);
			}
			else if(dif < 24){
				ball.velocity_x = mag*cos(1.04719755);
				ball.velocity_y = mag*sin(1.04719755);
			}
			else{
				ball.velocity_x = mag*cos(0.78539816);
				ball.velocity_y = mag*sin(0.78539816);
			}
			
			ball.y -= ball.velocity_y;
		break;
		case 2:
			if(ball.velocity_y > 0)
				ball.velocity_y = -ball.velocity_y;
			ball.y -= ball.velocity_y;
		break;
		case 3:
			if(ball.velocity_x < 0)
				ball.velocity_x = -ball.velocity_x;
			ball.x += ball.velocity_x;
		break;
		case 4:
			if(ball.velocity_x < 0)
				ball.velocity_x = -ball.velocity_x;
			if(ball.velocity_y < 0)
				ball.velocity_y = -ball.velocity_y;
			ball.x += ball.velocity_x;
			ball.y -= ball.velocity_y;
		break;
		case 5:
			if(ball.velocity_x < 0)
				ball.velocity_x = -ball.velocity_x;
			if(ball.velocity_y > 0)
				ball.velocity_y = -ball.velocity_y;
			ball.x += ball.velocity_x;
			ball.y -= ball.velocity_y;
		break;
		case 6:
			if(ball.velocity_x > 0)
				ball.velocity_x = -ball.velocity_x;
			ball.x += ball.velocity_x;
		break;
		case 7:
			if(ball.velocity_x > 0)
				ball.velocity_x = -ball.velocity_x;
			if(ball.velocity_y < 0)
				ball.velocity_y = -ball.velocity_y;
			ball.x += ball.velocity_x;
			ball.y -= ball.velocity_y;
		break;
		case 8:
			if(ball.velocity_x > 0)
				ball.velocity_x = -ball.velocity_x;
			if(ball.velocity_y > 0)
				ball.velocity_y = -ball.velocity_y;
			ball.x += ball.velocity_x;
			ball.y -= ball.velocity_y;
		break;
	}
}

//=============================================================================
uint8_t wallBounce(float oldX, float oldY)
{
	// see if we've hit the left wall
	if(ball.x <= 10){
		if(ball.velocity_x < 0)
			ball.velocity_x = -ball.velocity_x;
		ball.x = oldX;	
		return 1;
	}
	// right wall
	else if(ball.x >= 166-ball.width){
		if(ball.velocity_x > 0)
			ball.velocity_x = -ball.velocity_x;
		ball.x = oldX;
		return 1;
	}
	// top border
	if(ball.y <= 50){
		if(ball.velocity_y > 0)
			ball.velocity_y = -ball.velocity_y;
		ball.y = oldY;
		return 1;
	}
	// bottom of stage
	else if(ball.y >= 220-ball.height){
		
		while(spi_isLocked() == true){}	// make sure we don't break nothing
		
		// lose a life
		game.lives--;
		
		// do stuff to end game
		if(game.lives == 0 || game.lives > 10){

			if(game.score > game.highscore){	// set new highscore
				game.highscore = game.score;
			}
			game.score = 0;	// reset current score
			game.lives = 0; // reset lives
	
			// print the lives and death message
			video_print_one_seg(game.lives, SEG_COLOR, LIVES_OFFSET, 0);
			video_print_string("You suck, sucker", myfont, 25, 180, 0xF800, 0x0000);
			
			// set up death
			game.current = DEATH_STATE;
			beep = DEAD_BEEP;
			timers[BEEP_TIMER] = timer_get();
			return 0;
		}
		
		// re-initialize the board
		float mag = sqrt(ball.velocity_x*ball.velocity_x + ball.velocity_y*ball.velocity_y);
		
		ball.velocity_x = mag/sqrt(2);
		ball.velocity_y = ball.velocity_x;
		
		initialize_board(ball.velocity_x,ball.velocity_y,true);
	}
	return 0xFF;
}

//=============================================================================
void increaseSpeed()
{
	float mag = sqrt(ball.velocity_x*ball.velocity_x + ball.velocity_y*ball.velocity_y);

	mag += 1;
	
	ball.velocity_x = mag/sqrt(2);
	ball.velocity_y = ball.velocity_x;
}

//=============================================================================
void mode23_func(float magnitude, float x, float y)
{
	// set LED values for LED1 according to positive swing of joystick
	if(y > 0){
		uint16_t red;
		uint16_t green;
		uint16_t blue;
		
		if(y <= 0.166){
			red = 0;
			green = 0;
			blue = 0xFFF/0.166*y;
		}
		if(y > 0.166 && y <= 0.333){
			red = 0xFFF/0.166*(y-0.166);
			green = 0;
			blue = 0xFFF - 0xFFF/0.166*(y - 0.166);
		}
		if(y > 0.333 && y <= 0.5){
			red = 0xFFF;
			green = 0xFFF/0.166*(y-0.333);
			blue = 0;
		}
		if(y > 0.5 && y <= 0.666){
			red = 0xFFF - 0xFFF/0.166*(y - 0.5);
			green = 0xFFF;
			blue = 0;
		}
		if(y > 0.666 && y <= 0.833){
			red = 0;
			green = 0xFFF;
			blue = 0xFFF/0.166*(y-0.666);
		}
		if(y > 0.833 && y < 1){
			red = 0xFFF/0.166*(y-0.833);
			green = 0xFFF;
			blue = 0xFFF;
		}
		if(y >= 1){
			red = 0xFFF;
			green = 0xFFF;
			blue = 0xFFF;
		}
		LEDs[0] = red;
		LEDs[1] = green;
		LEDs[2] = blue;
	}
	// set LED values for LED2 according to positive swing of joystick
	if(x > 0){
		uint16_t red;
		uint16_t green;
		uint16_t blue;
		
		if(x <= 0.166){
			red = 0;
			green = 0;
			blue = 0xFFF/0.166*x;
		}
		if(x > 0.166 && x <= 0.333){
			red = 0xFFF/0.166*(x-0.166);
			green = 0;
			blue = 0xFFF - 0xFFF/0.166*(x - 0.166);
		}
		if(x > 0.333 && x <= 0.5){
			red = 0xFFF;
			green = 0xFFF/0.166*(x-0.333);
			blue = 0;
		}
		if(x > 0.5 && x <= 0.666){
			red = 0xFFF - 0xFFF/0.166*(x - 0.5);
			green = 0xFFF;
			blue = 0;
		}
		if(x > 0.666 && x <= 0.833){
			red = 0;
			green = 0xFFF;
			blue = 0xFFF/0.166*(x-0.666);
		}
		if(x > 0.833 && x < 1){
			red = 0xFFF/0.166*(x-0.833);
			green = 0xFFF;
			blue = 0xFFF;
		}
		if(x >= 1){
			red = 0xFFF;
			green = 0xFFF;
			blue = 0xFFF;
		}
		LEDs[3] = red;
		LEDs[4] = green;
		LEDs[5] = blue;
	}
	// set LED values for LED3 according to negative swing of joystick
	if(y < 0){
		uint16_t red;
		uint16_t green;
		uint16_t blue;
		
		if(y >= -0.166){
			red = 0;
			green = 0;
			blue = 0xFFF/0.166*-y;
		}
		if(y < -0.166 && y >= -0.333){
			red = 0xFFF/0.166*(-y-0.166);
			green = 0;
			blue = 0xFFF - 0xFFF/0.166*(-y - 0.166);
		}
		if(y < -0.333 && y >= -0.5){
			red = 0xFFF;
			green = 0xFFF/0.166*(-y-0.333);
			blue = 0;
		}
		if(y < -0.5 && y >= -0.666){
			red = 0xFFF - 0xFFF/0.166*(-y - 0.5);
			green = 0xFFF;
			blue = 0;
		}
		if(y < -0.666 && y >= -0.833){
			red = 0;
			green = 0xFFF;
			blue = 0xFFF/0.166*(-y-0.666);
		}
		if(y < -0.833 && y > -1){
			red = 0xFFF/0.166*(-y-0.833);
			green = 0xFFF;
			blue = 0xFFF;
		}
		if(y <= -1){
			red = 0xFFF;
			green = 0xFFF;
			blue = 0xFFF;
		}
		LEDs[6] = red;
		LEDs[7] = green;
		LEDs[8] = blue;
	}
	// set LED values for LED4 according to negative swing of joystick
	if(x < 0){
		uint16_t red;
		uint16_t green;
		uint16_t blue;
		
		if(x >= -0.166){
			red = 0;
			green = 0;
			blue = 0xFFF/0.166*-x;
		}
		if(x < -0.166 && x >= -0.333){
			red = 0xFFF/0.166*(-x-0.166);
			green = 0;
			blue = 0xFFF - 0xFFF/0.166*(-x - 0.166);
		}
		if(x < -0.333 && x >= -0.5){
			red = 0xFFF;
			green = 0xFFF/0.166*(-x-0.333);
			blue = 0;
		}
		if(x < -0.5 && x >= -0.666){
			red = 0xFFF - 0xFFF/0.166*(-x - 0.5);
			green = 0xFFF;
			blue = 0;
		}
		if(x < -0.666 && x >= -0.833){
			red = 0;
			green = 0xFFF;
			blue = 0xFFF/0.166*(-x-0.666);
		}
		if(x < -0.833 && x > -1){
			red = 0xFFF/0.166*(-x-0.833);
			green = 0xFFF;
			blue = 0xFFF;
		}
		if(x <= -1){
			red = 0xFFF;
			green = 0xFFF;
			blue = 0xFFF;
		}
		LEDs[9] = red;
		LEDs[10] = green;
		LEDs[11] = blue;
	}
	// set LED value for LED5 according to magnitude from center
	if(magnitude >= 0){
		uint16_t red;
		uint16_t green;
		uint16_t blue;
		magnitude = 1 - magnitude;
		
		if(magnitude <= 0.166){
			red = 0;
			green = 0;
			blue = 0xFFF/0.166*magnitude;
		}
		if(magnitude > 0.166 && magnitude <= 0.333){
			red = 0xFFF/0.166*(magnitude-0.166);
			green = 0;
			blue = 0xFFF - 0xFFF/0.166*(magnitude - 0.166);
		}
		if(magnitude > 0.333 && magnitude <= 0.5){
			red = 0xFFF;
			green = 0xFFF/0.166*(magnitude-0.333);
			blue = 0;
		}
		if(magnitude > 0.5 && magnitude <= 0.666){
			red = 0xFFF - 0xFFF/0.166*(magnitude - 0.5);
			green = 0xFFF;
			blue = 0;
		}
		if(magnitude > 0.666 && magnitude <= 0.833){
			red = 0;
			green = 0xFFF;
			blue = 0xFFF/0.166*(magnitude-0.666);
		}
		if(magnitude > 0.833 && magnitude < 1){
			red = 0xFFF/0.166*(magnitude-0.833);
			green = 0xFFF;
			blue = 0xFFF;
		}
		if(magnitude >= 1){
			red = 0xFFF;
			green = 0xFFF;
			blue = 0xFFF;
		}
		LEDs[12] = red;
		LEDs[13] = green;
		LEDs[14] = blue;
	}
	// write the new LED values
	double_toInt(LEDpwm, LEDs);
	tlc_write(LEDpwm);
	
	// clear the LED array for subsequent runs
	for(uint8_t i = 0; i < 16; i++){
		LEDs[i] = 0;
	}
}

//=============================================================================
void double_toInt(uint16_t intArr[], double doubleArr[])
{
	// cast the array of doubles to an array of uint's
	for(int i = 0; i < 16; i++){
		intArr[i] = (uint16_t) doubleArr[i];
	}
}

//-----------------------------------------------------------------------------
//        __   __   __
//     | /__` |__) /__`
//     | .__/ |  \ .__/
//
//-----------------------------------------------------------------------------
