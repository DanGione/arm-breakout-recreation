//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "adc.h"
#include <sam.h>
#include <math.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define ADC_CLOCK_GENERATOR (0)
#define ADC_SOURCE (ADC_INPUTCTRL_MUXPOS_PIN0_Val)

// get some defines for the joystick values
#define Middle (1988)
#define Top (24)
#define Bottom (4087)
#define UpScale (1.0/(Middle - Top))
#define DownScale (1.0/(Middle - Bottom))

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

static volatile uint16_t x = 0;
static volatile uint16_t y = 0;

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
void adc_init()
{
  // Set the config of the clock with CLKCTRL (must be one write only)
  GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID_ADC | GCLK_CLKCTRL_GEN(ADC_CLOCK_GENERATOR) | GCLK_CLKCTRL_CLKEN;

  // Set the input pin as input
  PORT->Group[1].DIRCLR.reg = PORT_PB08;
  PORT->Group[0].DIRCLR.reg = PORT_PA02;
  
  // Make sure the Power Manager is allowing the ADC its stuff
  PM->APBCMASK.bit.ADC_ = 1;
  
  // Select input and select reference as ground (INPUTCTRL) as well as set input scan
  ADC->INPUTCTRL.reg = ADC_INPUTCTRL_MUXNEG_GND | 
						ADC_INPUTCTRL_GAIN_DIV2 | 
						ADC_INPUTCTRL_MUXPOS(ADC_SOURCE) |
						ADC_INPUTCTRL_INPUTSCAN(3) |
						ADC_INPUTCTRL_INPUTOFFSET(0); 
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }

  ADC ->REFCTRL.bit.REFSEL = 0x2;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
  
  // Enable left-adjustment and free-running mode (CTRLB)
  ADC->CTRLB.reg = ADC_CTRLB_FREERUN | ADC_CTRLB_LEFTADJ | ADC_CTRLB_RESSEL_16BIT | ADC_CTRLB_PRESCALER_DIV32;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
	  
  // do some averaging
  ADC->AVGCTRL.reg = ADC_AVGCTRL_ADJRES(2) | ADC_AVGCTRL_SAMPLENUM_4;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
  
  // Enable ADC by writing CTRLA.ENABLE
  ADC->CTRLA.bit.ENABLE = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
  
  // Enable the result-ready interrupt
  ADC->INTENSET.bit.RESRDY = 1;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
  
  // Kick it to make sure it starts the first conversion
  ADC->SWTRIG.reg = ADC_SWTRIG_START;
  while (ADC->STATUS.bit.SYNCBUSY == 1) { }
	
  // Set up the interrupt handler
  NVIC_DisableIRQ(ADC_IRQn);
  NVIC_ClearPendingIRQ(ADC_IRQn);
  NVIC_SetPriority(ADC_IRQn, 0);
  NVIC_EnableIRQ(ADC_IRQn);
}

uint16_t adc_read(uint8_t axis)
{
	if(axis == XAXIS)
		return x;
	else if(axis == YAXIS)
		return y;
	else
		return 0xFFFF;
}

double findModifier(uint8_t axis)
{
	// get the 12 bit, left-adjusted adc value into a 16 bit right-adjusted uint
	uint16_t adc = adc_read(axis);
	double retval = 0;
	
	// return the wawa-ed value depending on joystick direction
	if(adc > Middle){
		retval = -1 + (adc - Bottom)*DownScale;
		if(retval <= -0.99)
			retval = -1;
		if(retval >= -0.02)
			retval = 0;
	}
	else if(adc < Middle){
		retval = 1 + (Top - adc)*UpScale;
		if(retval >= 0.99)
			retval = 1;
		if(retval <= 0.02)
			retval = 0;
	}
	
	return retval;
}


double findMagnitude()
{
	// get the two -1 to 1 values of the axes
	double x = findModifier(XAXIS);
	double y = findModifier(YAXIS);
	
	double retval = 0;
	
	// find the magnitude
	retval = sqrt(x*x + y*y);
	
	// ensure it never goes above 1
	if(retval > 1)
		retval = 1;
	
	return retval;
}

uint8_t findQuad()
{
	// get the two -1 to 1 values of the axes
	double x = findModifier(XAXIS);
	double y = findModifier(YAXIS);
	
	double retval = 0;
	
	// find the current quadrant the joystick is pointing in
	if(x >= 0 && y >= 0)
		retval = 1;
	else if(x < 0 && y >= 0)
		retval = 2;
	else if(x < 0 && y < 0)
		retval = 3;
	else if(x >= 0 && y < 0)
		retval = 4;
		
	return retval;
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`   
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------

void ADC_Handler(void)
{
  // Check whether the interrupt was fired because the result is ready
  if (ADC->INTFLAG.bit.RESRDY)
  {
	while (ADC->STATUS.bit.SYNCBUSY == 1) { }
		
    // Read result
	if(ADC->INPUTCTRL.bit.INPUTOFFSET == 1)	// checks for being AIN0 (yes, I know. It shouldn't but it do)
		x = ADC->RESULT.bit.RESULT;
	else if(ADC->INPUTCTRL.bit.INPUTOFFSET == 3) // checks for being AIN2 (again, not perfect but it works)
		y = ADC->RESULT.bit.RESULT;
  }
}