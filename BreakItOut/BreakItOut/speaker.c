//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "speaker.h"
#include <sam.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define SPEAKER (PORT_PA09)	//TC3/WO[1]  TCC0/WO[5]
#define SPEAKER_GROUP (0)
#define SPEAKER_PIN (PIN_PA09%32)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------


//==============================================================================

void speaker_init()
{
	// enable peripheral mux on the LEDs and set them to the needed functions
	PORT->Group[SPEAKER_GROUP].PINCFG[SPEAKER_PIN].bit.PMUXEN = 1;

	PORT->Group[SPEAKER_GROUP].PMUX[SPEAKER_PIN >> 1].bit.PMUXO = PORT_PMUX_PMUXO_F_Val;
	
	// turn on TCC1 in the power manager and set clock source
	PM->APBCMASK.reg |= PM_APBCMASK_TCC1;
	GCLK->CLKCTRL.reg = (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID(TCC1_GCLK_ID));
	while (GCLK->STATUS.bit.SYNCBUSY) {}
	 
	// reset TCC1 and set direction of count
	TCC1->CTRLA.reg = TCC_CTRLA_SWRST;
	while (TCC1->SYNCBUSY.reg & TCC_SYNCBUSY_SWRST) {}
	TCC1->CTRLBCLR.reg = TCC_CTRLBCLR_DIR;				// count up
	while (TCC1->SYNCBUSY.reg & TCC_SYNCBUSY_CTRLB) {}
	
	// set the prescalar to divide by 1
	TCC1->CTRLA.reg = (TCC_CTRLA_PRESCSYNC_GCLK_Val | TCC_CTRLA_PRESCALER(TCC_CTRLA_PRESCALER_DIV1_Val));
	// select the waveform generation mode -> normal PWM
	TCC1->WAVE.reg = (TCC_WAVE_WAVEGEN_MFRQ);
	while (TCC1->SYNCBUSY.reg & TCC_SYNCBUSY_WAVE) {}
		
	TCC1->WEXCTRL.bit.OTMX = 0x2;
		
	// set the initial period to zero (close to it)
	REG_TCC1_CCB0 = 0xffffff;
	while (TCC1->SYNCBUSY.reg & TCC_SYNCBUSY_CC0) {}
	// start speaker operation
	TCC1->CTRLA.reg |= (TCC_CTRLA_ENABLE);
}

void speaker_out(double newFrequency)
{
	uint32_t newPeriod;
	if(newFrequency >= 1)
		newPeriod = 48000000UL / newFrequency;
	else
		newPeriod = 20;
		
	// divide frequency by 2 because it's MFRQ
	REG_TCC1_CCB0 = newPeriod / 2;
}

void speaker_shutup(bool shutup)
{
	PORT->Group[SPEAKER_GROUP].PINCFG[SPEAKER_PIN].bit.PMUXEN = !shutup;
}


//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------
