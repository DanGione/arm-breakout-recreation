//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "tlc_counter.h"
#include "sam.h"
#include "tlc5940.h"
#include <stdbool.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define LED_LATCH (PORT_PA14)
#define LED_BLANK (PORT_PA07)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

bool needLatch = false;

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//==============================================================================
void tlc_counter_init()
{
	// Enable the bus clk for the peripheral
	PM->APBCMASK.bit.TC3_ = 1;
	
	// Configure the General Clock with the 48MHz clk
	GCLK->CLKCTRL.reg = GCLK_CLKCTRL_ID(GCLK_CLKCTRL_ID_TCC2_TC3) |
	GCLK_CLKCTRL_GEN_GCLK0 |
	GCLK_CLKCTRL_CLKEN;
	// Wait for the GCLK to be synchronized
	while(GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY);
	
	tlc_counter_disable();

	// Put it in the 16-bit mode.
	TC3->COUNT16.CTRLA.bit.MODE = TC_CTRLA_MODE_COUNT16_Val;

	// Set up for normal frequency mode (count to period)
	TC3->COUNT16.CTRLA.bit.WAVEGEN = TC_CTRLA_WAVEGEN_MFRQ_Val;
	
	// Setup count event inputs
	TC3->COUNT16.EVCTRL.bit.EVACT = TC_EVCTRL_EVACT_COUNT_Val;
	
	// Enable the event input
	TC3->COUNT16.EVCTRL.bit.TCEI = 1;
	
	// Set the Period to be 8192 toggles of GSCLK (4096 periods)
	tlc_counter_set(8191);
	
	TC3->COUNT16.INTENSET.bit.OVF = 1;
	NVIC_EnableIRQ(TC3_IRQn);
	
	// set up LED Blank and Latch as output and set them low
	REG_PORT_DIR0 |= LED_BLANK;
	REG_PORT_DIR0 |= LED_LATCH;
	REG_PORT_OUTCLR0 = LED_BLANK;
	REG_PORT_OUTCLR0 = LED_LATCH;
}

//============================================================================
void tlc_counter_set(uint16_t value)
{
	// Set the Period to be 10 Events
	TC3->COUNT16.CC[0].reg = value;
	while (TC3->COUNT8.STATUS.bit.SYNCBUSY);
}

//============================================================================
void tlc_counter_enable()
{
	TC3->COUNT16.CTRLA.bit.ENABLE = 1;
	while (TC3->COUNT16.STATUS.bit.SYNCBUSY);

}

//============================================================================
void tlc_counter_disable()
{
	TC3->COUNT16.CTRLA.bit.ENABLE = 0;
	while (TC3->COUNT16.STATUS.bit.SYNCBUSY);
}

void latch_plz()
{
	needLatch = true;
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------
TC3_Handler()
{
	// toggle LED BLANK
	REG_PORT_OUTTGL0 = LED_BLANK;
	REG_PORT_OUTTGL0 = LED_BLANK;
	
	if(needLatch){
		REG_PORT_OUTTGL0 = LED_LATCH;
		REG_PORT_OUTTGL0 = LED_LATCH;
		needLatch = false;
		SERCOM4->SPI.INTENCLR.bit.TXC = 1;
		tlc_unlock();
		spi_unlock();
	}
	
	TC3->COUNT16.INTFLAG.bit.OVF = 1;
}