//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "buttons.h"
#include <sam.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

#define BUTTON_ACTIVE (PORT_PA08)
#define BUTTON_ACTIVE_GROUP (0)
#define BUTTON_ACTIVE_PIN (PIN_PA08%32)

#define BUTTON_0 (PORT_PB09)
#define BUTTON_0_GROUP (1)
#define BUTTON_0_PIN (PIN_PB09%32)

#define BUTTON_1 (PORT_PA04)
#define BUTTON_1_GROUP (0)
#define BUTTON_1_PIN (PIN_PA04%32)

#define BUTTON_JOYSTICK (PORT_PA20)
#define BUTTON_JOYSTICK_GROUP (0)
#define BUTTON_JOYSTICK_PIN (PIN_PA20%32)

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

bool joystickButton = false;
bool joyRising = false;
bool joyFalling = false;
bool btn0 = false;
bool btn1 = false;
bool btnact = false;

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

//=============================================================================
void buttons_init(void)
{
	
	// Turn on the input enables for each button
	PORT->Group[BUTTON_ACTIVE_GROUP].PINCFG[BUTTON_ACTIVE_PIN].bit.INEN = 1;
	PORT->Group[BUTTON_JOYSTICK_GROUP].PINCFG[BUTTON_JOYSTICK_PIN].bit.INEN = 1;
	PORT->Group[BUTTON_0_GROUP].PINCFG[BUTTON_0_PIN].bit.INEN = 1;
	PORT->Group[BUTTON_1_GROUP].PINCFG[BUTTON_1_PIN].bit.INEN = 1;
  
	// Make sure the directions regs are configured as inputs
	PORT->Group[BUTTON_ACTIVE_GROUP].DIRCLR.reg = BUTTON_ACTIVE;
	PORT->Group[BUTTON_JOYSTICK_GROUP].DIRCLR.reg = BUTTON_JOYSTICK;
	PORT->Group[BUTTON_0_GROUP].DIRCLR.reg = BUTTON_0;
	PORT->Group[BUTTON_1_GROUP].DIRCLR.reg = BUTTON_1;
		  
	// set up external interrupt on Joystick button
	PORT->Group[BUTTON_JOYSTICK_GROUP].PINCFG[BUTTON_JOYSTICK_PIN].bit.PMUXEN = 1;
	PORT->Group[BUTTON_JOYSTICK_GROUP].PMUX[BUTTON_JOYSTICK_PIN >> 1].bit.PMUXE = PORT_PMUX_PMUXE_A_Val;
	
	// set up external interrupt on other buttons
	PORT->Group[BUTTON_ACTIVE_GROUP].PINCFG[BUTTON_ACTIVE_PIN].bit.PMUXEN = 1;
	PORT->Group[BUTTON_ACTIVE_GROUP].PMUX[BUTTON_ACTIVE_PIN >> 1].bit.PMUXE = PORT_PMUX_PMUXE_A_Val;

	// turn on generic clock 1 (32kHz oscillator)
	SYSCTRL->XOSC32K.bit.XTALEN = 1;
	SYSCTRL->XOSC32K.bit.ENABLE = 1;

	// set EIC to use GCLK1 and turn it on in PM
	PM->APBCMASK.reg |= PM_APBAMASK_EIC;
	GCLK->CLKCTRL.reg = (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK1 | GCLK_CLKCTRL_ID(EIC_GCLK_ID));
	while (GCLK->STATUS.bit.SYNCBUSY) {}
	
	EIC->CTRL.reg = EIC_CTRL_SWRST;		// reset the EIC
	while (EIC->STATUS.bit.SYNCBUSY) {}
	REG_EIC_INTENSET |= EIC_INTENSET_EXTINT4;		// turn on interrupts on extint 4
	while (EIC->STATUS.bit.SYNCBUSY) {}
	REG_EIC_CONFIG0 |= EIC_CONFIG_FILTEN4 | EIC_CONFIG_SENSE4_BOTH;	// sense Joystick on both edges and filter it
	while (EIC->STATUS.bit.SYNCBUSY) {}
	REG_EIC_NMICTRL |= EIC_NMICTRL_NMIFILTEN | EIC_NMICTRL_NMISENSE_BOTH;// sense NMI on both edges and filter it
	while (EIC->STATUS.bit.SYNCBUSY) {}
	REG_EIC_CTRL |= EIC_CTRL_ENABLE;		// release the floodgates
	while (EIC->STATUS.bit.SYNCBUSY) {}

	// turn on EIC_Handler
	NVIC_EnableIRQ(EIC_IRQn);
}

//==============================================================================
// Inputs are active high
//==============================================================================
bool buttons_get(uint8_t button)
{
	// set up return value and get button states
	bool retval = false;
	
	// return joystick button if asked for
	if(button == 0x04)
		retval = joystickButton;
	
	// if one of the 4 pushbuttons is pressed (and not asking for joystick button)
	// return the value of the requested button
	else if(btnact == 1)
	{	
		if(button == 0x00 && (!btn0 & !btn1) == 1)
			retval = true;
		else if(button == 0x01 && (btn0 & !btn1) == 1)
			retval = true;
		else if(button == 0x02 && (!btn0 & btn1) == 1)
			retval = true;
		else if(button == 0x03 && (btn0 & btn1) == 1)
			retval = true;
	}
  
	return retval;	// return the requested value
}

// deletes rising/falling edge value as soon as it is read.
bool get_risingEdge(uint8_t button)
{
	bool retval = false;
	
	if(button == 0x04){
		retval = joyRising;
		joyRising = false;
	}
		
	return retval;
}

//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`   
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------

void EIC_Handler()
{
	if(REG_EIC_INTFLAG & 0x10){
		joystickButton = !(PORT->Group[BUTTON_JOYSTICK_GROUP].IN.reg & (BUTTON_JOYSTICK));
		
		// determine if it's a rising or falling edge
		if(joystickButton){
			joyRising = true;
			joyFalling = false;
		}
		else{
			joyRising = false;
			joyFalling = true;
		}
			
		EIC->INTFLAG.bit.EXTINT4 = 1;
	}
}

void NonMaskableInt_Handler()
{
	if(REG_EIC_NMIFLAG & 0x01){
		btnact = (PORT->Group[BUTTON_ACTIVE_GROUP].IN.reg & (BUTTON_ACTIVE));
		
		btn1 = (PORT->Group[BUTTON_1_GROUP].IN.reg & (BUTTON_1));
		btn0 = (PORT->Group[BUTTON_0_GROUP].IN.reg & (BUTTON_0));
		
		EIC->NMIFLAG.bit.NMI = 1;
	}
}