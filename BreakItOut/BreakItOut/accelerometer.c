//------------------------------------------------------------------------------
//             __             __   ___  __
//     | |\ | /  ` |    |  | |  \ |__  /__`
//     | | \| \__, |___ \__/ |__/ |___ .__/
//
//------------------------------------------------------------------------------

#include "delay.h"
#include "accelerometer.h"
#include "bmi160.h"
#include "i2c.h"
#include <stdint.h>

//------------------------------------------------------------------------------
//      __   ___  ___         ___  __
//     |  \ |__  |__  | |\ | |__  /__`
//     |__/ |___ |    | | \| |___ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//     ___      __   ___  __   ___  ___  __
//      |  \ / |__) |__  |  \ |__  |__  /__`
//      |   |  |    |___ |__/ |___ |    .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//                __          __        ___  __
//     \  /  /\  |__) |  /\  |__) |    |__  /__`
//      \/  /~~\ |  \ | /~~\ |__) |___ |___ .__/
//
//------------------------------------------------------------------------------

volatile int16_t axisVals[3];
bool newVals = false;

uint8_t data[32];
uint8_t address;
struct bmi160_dev sensor;
struct bmi160_sensor_data accel;
uint8_t retval;

//------------------------------------------------------------------------------
//      __   __   __  ___  __  ___      __   ___  __
//     |__) |__) /  \  |  /  \  |  \ / |__) |__  /__`
//     |    |  \ \__/  |  \__/  |   |  |    |___ .__/
//
//------------------------------------------------------------------------------

void accelerometer_grab();
void user_delay_ms(uint32_t ms);
int8_t user_i2c_read(uint8_t dev_addr, uint8_t reg_addr,
uint8_t *data, uint16_t length);
int8_t user_i2c_write(uint8_t dev_addr, uint8_t reg_addr,
uint8_t *data, uint16_t length);

//------------------------------------------------------------------------------
//      __        __          __
//     |__) |  | |__) |    | /  `
//     |    \__/ |__) |___ | \__,
//
//------------------------------------------------------------------------------

void accelerometer_init()
{
	sensor.id = BMI160_I2C_ADDR;
	sensor.interface = BMI160_I2C_INTF;
	sensor.read = user_i2c_read;
	sensor.write = user_i2c_write;
	sensor.delay_ms = user_delay_ms;
	int8_t rslt = BMI160_OK;
	
	NVIC_DisableIRQ(SERCOM3_IRQn);
	
	retval = bmi160_init(&sensor);
	/* After the above function call, accel and gyro parameters in the device structure
	are set with default values, found in the datasheet of the sensor */
	
	/* Select the Output data rate, range of accelerometer sensor */
	sensor.accel_cfg.odr = BMI160_ACCEL_ODR_1600HZ;
	sensor.accel_cfg.range = BMI160_ACCEL_RANGE_2G;
	sensor.accel_cfg.bw = BMI160_ACCEL_BW_NORMAL_AVG4;
	/* Select the power mode of accelerometer sensor */
	sensor.accel_cfg.power = BMI160_ACCEL_NORMAL_MODE;
	
	/* Set the sensor configuration */
	rslt = bmi160_set_sens_conf(&sensor);
	
	SERCOM3->I2CM.INTENSET.bit.MB = 1;
	SERCOM3->I2CM.INTENSET.bit.SB = 1;
	NVIC_ClearPendingIRQ(SERCOM3_IRQn);
	NVIC_SetPriority(SERCOM3_IRQn, 3);
	NVIC_EnableIRQ(SERCOM3_IRQn);
}

// start i2c read stuff
bool accelerometer_update()
{
	if(read_accelerometer(accelerometer_grab)){
		newVals = true;
		return false;
	}
	else{
		return true;
	}	
}

// get the current values
int16_t accelerometer_get(uint8_t axis)
{
	return axisVals[axis];
}

double accelerometer_findModifier(uint8_t axis)
{
	// get the current axis value
	int16_t blah = accelerometer_get(axis);
	double retval = 0;
	
	// return the unit-scaled value (1 to -1)
	if(blah > 0){
		retval = blah/16000.0;
		if(retval >= 0.98)
			retval = 1;
		if(retval <= 0.015)
			retval = 0;
	}
	else if(blah < 0){
		retval = blah/16000.0;
		if(retval <= -0.98)
			retval = -1;
		if(retval >= -0.015)
			retval = 0;
	}
	
	return retval;
}


double accelerometer_findMagnitude()
{
	// get the two -1 to 1 values of the axes
	double x = accelerometer_findModifier(X_VAL);
	double y = accelerometer_findModifier(Y_VAL);
	
	double retval = 0;
	
	// find the magnitude
	retval = sqrt(x*x + y*y);
	
	// ensure it never goes above 1
	if(retval > 1)
		retval = 1;
	
	return retval;
}


//------------------------------------------------------------------------------
//      __   __              ___  ___
//     |__) |__) | \  /  /\   |  |__
//     |    |  \ |  \/  /~~\  |  |___
//
//------------------------------------------------------------------------------

void accelerometer_grab()
{
	get_accelerometer(axisVals);
}

//=============================================================================
void user_delay_ms(uint32_t ms)
{
	DelayMs(ms);
}

//=============================================================================
int8_t user_i2c_read(uint8_t dev_addr, uint8_t reg_addr,
uint8_t *data, uint16_t length)
{
	i2c_read_setup(dev_addr << 1, &reg_addr, 1);
	i2c_read(dev_addr << 1,data,length);
	return BMI160_OK;
}

//=============================================================================
int8_t user_i2c_write(uint8_t dev_addr, uint8_t reg_addr,
uint8_t *data, uint16_t length)
{
	uint8_t mydata[32];
	if (length > 31) length = 31;
	// Add 1 to include the reg address
	length++;
	//Send the address
	mydata[0] = reg_addr;
	bcopy(data, mydata+1, length);
	i2c_write(dev_addr << 1,mydata,length);
	return BMI160_OK;
}

//------------------------------------------------------------------------------
//      __                  __        __        __
//     /  `  /\  |    |    |__)  /\  /  ` |__/ /__`
//     \__, /~~\ |___ |___ |__) /~~\ \__, |  \ .__/
//
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//        __   __  , __
//     | /__` |__)  /__`   
//     | .__/ |  \  .__/
//
//------------------------------------------------------------------------------
